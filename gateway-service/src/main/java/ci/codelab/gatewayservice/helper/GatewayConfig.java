//package ci.codelab.gatewayservice.helper;
//
//import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
//import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
//import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class GatewayConfig {
//
//	
//	// ne doit pas etre une methode final ou private
//	@Bean
//	public DiscoveryClientRouteDefinitionLocator dynamicRoutes(ReactiveDiscoveryClient rdc,
//			DiscoveryLocatorProperties dlp){
//		return new DiscoveryClientRouteDefinitionLocator(rdc,dlp);
//	}
//
//}
