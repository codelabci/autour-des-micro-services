package ci.codelab.billingservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ci.codelab.billingservice.dto.Inventory;

@FeignClient(name = "inventory-service")
public interface InventoryRestClientService {

    @GetMapping("/inventories/{id}")
    public Inventory productById(@PathVariable Integer id);
    @GetMapping("/inventories")
    public PagedModel<Inventory> allInventories();
}
