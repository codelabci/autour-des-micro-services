package ci.codelab.billingservice.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ci.codelab.billingservice.dto.Customer;

@FeignClient(name = "customer-service")
public interface CustomerRestClientService {

//    @GetMapping("/customers/{id}?projection=allCustomer")
    @GetMapping("/customers/{id}")
    public Customer customerById(@PathVariable Integer id);
//    @GetMapping("/customers?projection=allCustomer")
    @GetMapping("/customers")
    public PagedModel<Customer> allCustomers();

}
