package ci.codelab.billingservice;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import ci.codelab.billingservice.dao.entity.Bill;
import ci.codelab.billingservice.dao.entity.InventoryItem;
import ci.codelab.billingservice.dao.repository.BillRepository;
import ci.codelab.billingservice.dao.repository.InventoryItemRepository;
import ci.codelab.billingservice.dto.Customer;
import ci.codelab.billingservice.dto.Inventory;
import ci.codelab.billingservice.service.CustomerRestClientService;
import ci.codelab.billingservice.service.InventoryRestClientService;

@SpringBootApplication
@EnableFeignClients // pour activer les clients rest avec Feign
public class BillingServiceApplication {
	
	private int customerId = 1 ;
	private int inventoryId = 1 ;

	public static void main(String[] args) {
		SpringApplication.run(BillingServiceApplication.class, args);
	}
	
	
	
//	@Bean
//	CommandLineRunner start(BillRepository billRepository, InventoryItemRepository inventoryItemRepository){
//		return args -> {
//			System.out.println(":::::::::: BEGIN InitData ::::::::");
//			Stream.of("youstra", "ticou", "younous", "traore").forEach(customerUserName->{
//
//				Bill bill = Bill.builder().billDate(new Date()).customerUserName(customerUserName).build() ;
//				billRepository.save(bill) ;
//
//				Bill billSaved = billRepository.save(bill) ;
//				Stream.of("Macbook pro M3 Max", "Macbook pro M3 Pro", "Macbook pro M3", "Macbook pro M2 Max", 
//						"Macbook pro M2 Pro","Macbook pro M2", "Macbook pro M1 Max", "Macbook pro M1 Pro", 
//						"Macbook pro M1").forEach(inventoryName->{
//							InventoryItem inventoryItem = InventoryItem.builder().bill(billSaved).price(12000000.0).inventoryName(inventoryName).build() ;
//							inventoryItemRepository.save(inventoryItem) ;
//
//						}) ;
//
//			});
//		};
//	}
//
	
	@Bean
	CommandLineRunner start(
			BillRepository billRepository,
			InventoryItemRepository inventoryItemRepository,
			CustomerRestClientService customerRestClientService,
			InventoryRestClientService inventoryRestClientService){
		return args -> {
			System.out.println(":::::::::: BEGIN InitData ::::::::");
			Random random=new Random();

//			List<Customer> customers=customerRestClientService.allCustomers().getContent().stream().toList(); size = 4
//			List<Inventory> products=inventoryRestClientService.allInventories().getContent().stream().toList();  size = 9
			
			
			customerRestClientService.allCustomers().getContent().stream().forEach(customer->{
				Bill bill = Bill.builder().billDate(new Date())
						.customerUserName(customer.getUserName())
						.customerId(customerId).build() ;
				billRepository.save(bill) ;

				Bill billSaved = billRepository.save(bill) ;
				customerId ++ ;
				
				inventoryId = 1 ;
				inventoryRestClientService.allInventories().getContent().stream().forEach(inventory->{
					System.out.println(":::::::::: inventory InitData ::::::::" +inventory);

							InventoryItem inventoryItem = InventoryItem.builder().bill(billSaved)
									.quantity(1+random.nextInt(10)).price(inventory.getPrice())
//									.inventoryId(inventory.getId())
									.inventoryId(inventoryId)
									.inventoryName(inventory.getName()).build() ;
							inventoryItemRepository.save(inventoryItem) ;
							
							inventoryId ++ ;


						}) ;

			});
		};
	}
}


