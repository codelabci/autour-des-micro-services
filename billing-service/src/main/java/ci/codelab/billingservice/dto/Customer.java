package ci.codelab.billingservice.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Customer{

	private Integer id;
	
	
	private String nom ;
	private String prenoms ;
	private String userName ;
	private String email ;
}
