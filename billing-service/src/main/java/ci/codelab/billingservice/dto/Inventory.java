package ci.codelab.billingservice.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class  Inventory {

	
	private Integer id ;

	private String name ;
	private Double price ;
}


