package ci.codelab.billingservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ci.codelab.billingservice.dao.entity.Bill;
import ci.codelab.billingservice.dao.repository.BillRepository;
import ci.codelab.billingservice.dao.repository.InventoryItemRepository;
import ci.codelab.billingservice.dto.Customer;
import ci.codelab.billingservice.dto.Inventory;
import ci.codelab.billingservice.service.CustomerRestClientService;
import ci.codelab.billingservice.service.InventoryRestClientService;

@RestController
public class BillController {
	
	private BillRepository billRepository;
    private InventoryItemRepository inventoryItemRepository;
    private CustomerRestClientService customerRestClientService;
    private InventoryRestClientService inventoryRestClientService;

    public BillController(BillRepository billRepository, InventoryItemRepository inventoryItemRepository, CustomerRestClientService customerRestClientService, InventoryRestClientService inventoryRestClientService) {
        this.billRepository = billRepository;
        this.inventoryItemRepository = inventoryItemRepository;
        this.customerRestClientService = customerRestClientService;
        this.inventoryRestClientService = inventoryRestClientService;
    }

    @GetMapping("/fullOrder/{id}")
    public Bill getOrder(@PathVariable Integer id){
        Bill bill=billRepository.findById(id).get();
        Customer customer=customerRestClientService.customerById(bill.getCustomerId());
        bill.setCustomer(customer);
        
		System.out.println(":::::::::: bill.getInventoryItems().size() :::::::: " + bill.getInventoryItems().size());

        
        
        bill.getInventoryItems().forEach(currentInventory->{
            Inventory inventory=inventoryRestClientService.productById(currentInventory.getInventoryId());
            currentInventory.setInventory(inventory) ;
        });
        return bill;
    }

}
