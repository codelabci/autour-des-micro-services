package ci.codelab.billingservice.dao.entity;

import java.util.Date;
import java.util.List;

import ci.codelab.billingservice.dto.Customer;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Bill {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private Date billDate ;
	
	private String customerUserName;
	private Integer customerId;
	

//    @OneToMany(fetch = FetchType.EAGER, mappedBy = "bill")
    @OneToMany(mappedBy = "bill")
	private List<InventoryItem> inventoryItems;
	
	@Transient
	private  Customer customer ; 

}
