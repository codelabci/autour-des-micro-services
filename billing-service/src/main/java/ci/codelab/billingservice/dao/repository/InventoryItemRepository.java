package ci.codelab.billingservice.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import ci.codelab.billingservice.dao.entity.Bill;
import ci.codelab.billingservice.dao.entity.InventoryItem;

@RepositoryRestResource
public interface InventoryItemRepository extends JpaRepository<InventoryItem, Integer>{
	List<InventoryItem> findByBillId(Integer billId) ;
}
