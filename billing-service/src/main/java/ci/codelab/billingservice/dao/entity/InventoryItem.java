package ci.codelab.billingservice.dao.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ci.codelab.billingservice.dto.Customer;
import ci.codelab.billingservice.dto.Inventory;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InventoryItem {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private	Integer id ;

	private Integer quantity ;
	private Double price ;
	
	private String inventoryName;
	private Integer inventoryId;

	
	@ManyToOne
    @JoinColumn(name = "bill_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Bill bill;

	
	@Transient
	private  Inventory inventory ; 
}
