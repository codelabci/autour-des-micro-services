//package ci.codelab.billingservice.dao.repository;
//
//import java.util.Date;
//import java.util.stream.Stream;
//
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import ci.codelab.billingservice.dao.entity.Bill;
//import ci.codelab.billingservice.dao.entity.InventoryItem;
//
//
//@Configuration
//public class InitData {
//
//
//	//	@Bean
//	//	public void 
//
//
//	@Bean
//	CommandLineRunner start(BillRepository billRepository, InventoryItemRepository inventoryItemRepository){
//		return args -> {
//			System.out.println(":::::::::: BEGIN InitData ::::::::");
//			Stream.of("youstra", "ticou", "younous", "traore").forEach(customerUserName->{
//
//				Bill bill = Bill.builder().billDate(new Date()).customerUserName(customerUserName).build() ;
//				billRepository.save(bill) ;
//
//				Bill billSaved = billRepository.save(bill) ;
//				Stream.of("Macbook pro M3 Max", "Macbook pro M3 Pro", "Macbook pro M3", "Macbook pro M2 Max", 
//						"Macbook pro M2 Pro","Macbook pro M2", "Macbook pro M1 Max", "Macbook pro M1 Pro", 
//						"Macbook pro M1").forEach(inventoryName->{
//							InventoryItem inventoryItem = InventoryItem.builder().bill(billSaved).price(12000000.0).inventoryName(inventoryName).build() ;
//							inventoryItemRepository.save(inventoryItem) ;
//
//						}) ;
//
//			});
//			billRepository.findAll().forEach(System.out::println); // execute le toString
//			inventoryItemRepository.findAll().forEach(System.out::println); // execute le toString
//		};
//	}
//
//}
