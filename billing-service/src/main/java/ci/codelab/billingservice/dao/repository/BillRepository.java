package ci.codelab.billingservice.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import ci.codelab.billingservice.dao.entity.Bill;

@RepositoryRestResource
public interface BillRepository extends JpaRepository<Bill, Integer>{

}
