package ci.codelab.customerservice.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ci.codelab.customerservice.dao.entity.Customer;
import ci.codelab.customerservice.dto.CustomerProjection;


@RepositoryRestResource
//@RepositoryRestResource(excerptProjection = CustomerProjection.class)
public interface CustomerRepository extends JpaRepository<Customer, Integer>{
	
	
	
//	@GetMapping("/{id}")
//    public ResponseEntity<CustomerProjection> getUserById(@PathVariable Integer id) {
//        UserProjection userProjection = userRepository.findProjectedById(id, UserProjection.class);
//        if (userProjection != null) {
//            return ResponseEntity.ok(userProjection);
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }

}
