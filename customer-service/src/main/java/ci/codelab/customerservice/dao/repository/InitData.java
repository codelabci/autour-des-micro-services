package ci.codelab.customerservice.dao.repository;

import java.util.stream.Stream;

import org.springframework.context.annotation.Configuration;

import ci.codelab.customerservice.dao.entity.Customer;

@Configuration
public class InitData {
	
	InitData(CustomerRepository customerRepository){
		System.out.println(":::::::::: BEGIN InitData ::::::::");
		Stream.of("youstra", "ticou", "younous", "traore").forEach(cn->{
			customerRepository.save(new Customer(null, "Coulibaly", cn, cn, cn, cn  +"@gmail.com"));
		});
		customerRepository.findAll().forEach(System.out::println); // execute le toString
	}

}
