package ci.codelab.customerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableDiscoveryClient ici on dans le properties
//@EnableSwagger2
public class CustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerServiceApplication.class, args);
	}



	//	@Bean
	//	CommandLineRunner generate(CustomerRepository customerRepository) {
	//		
	//		
	//		return args -> {
	//			Stream.of("youstra", "ticou", "younous", "traore").forEach(cn->{
	//				customerRepository.save(new Customer(null, "Coulibaly", cn, cn, cn, cn  +"@gmail.com"));
	//			});
	//			customerRepository.findAll().forEach(System.out::println); // execute le toString
	//		};
	//	}


}
