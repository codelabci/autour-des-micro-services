package ci.codelab.customerservice.dto;

import org.springframework.data.rest.core.config.Projection;

import ci.codelab.customerservice.dao.entity.Customer;

@Projection(name="allCustomer", types= {Customer.class})
public interface CustomerProjection {
	
	Integer getId() ;
	String getPrenoms() ;
	String getUserName() ;
	String getNom() ;
	String getEmail() ;

}
