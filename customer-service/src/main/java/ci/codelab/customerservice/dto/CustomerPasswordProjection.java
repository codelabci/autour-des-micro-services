package ci.codelab.customerservice.dto;

import org.springframework.data.rest.core.config.Projection;

import ci.codelab.customerservice.dao.entity.Customer;


@Projection(name="passwordProjection", types= {Customer.class})
public interface CustomerPasswordProjection {

	String getPassword() ;

}
