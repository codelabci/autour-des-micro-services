package ci.codelab.inventoryservice.dao.repository;

import java.util.stream.Stream;

import org.springframework.context.annotation.Configuration;

import ci.codelab.inventoryservice.dao.entity.Inventory;

@Configuration
public class InitData {

	InitData(InventoryRepository inventoryRepository){
		System.out.println(":::::::::: BEGIN InitData ::::::::");
		Stream.of("Macbook pro M3 Max", "Macbook pro M3 Pro", "Macbook pro M3", "Macbook pro M2 Max", "Macbook pro M2 Pro",
				"Macbook pro M2", "Macbook pro M1 Max", "Macbook pro M1 Pro", "Macbook pro M1").forEach(cn->{
			inventoryRepository.save(Inventory.builder().name(cn).price(12000000.0).build());
		});
		inventoryRepository.findAll().forEach(System.out::println); // execute le toString
	}

}
