package ci.codelab.inventoryservice.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import ci.codelab.inventoryservice.dao.entity.Inventory;

@RepositoryRestResource
public interface InventoryRepository extends JpaRepository<Inventory, Integer>{

}
